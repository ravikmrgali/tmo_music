import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';

import Menu from './components/Menu';
import Login from './components/Login';
import Home from './components/Home';
import Dashboard from './components/Dashboard';
import About from './components/About';
import Contact from './components/Contact';
import Notfound from './components/Notfound';

function App() {
  return (
    <div className="App">
      <React.Fragment>
        <Router>
            <Menu />
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/login' component={Login} />
              <Route activeClassName="active" path='/dashboard' component={Dashboard} />
              <Route activeClassName="active" path='/about' component={About} />
              <Route activeClassName="active" path='/contact' component={Contact} />
              <Route component={Notfound} />
            </Switch>
        </Router>
      </React.Fragment>
    </div>
  );
}

export default App;
