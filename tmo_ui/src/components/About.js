import React from 'react';
import $ from 'jquery';
import {Container, Row, Col} from 'react-bootstrap';

class About extends React.Component {
    componentDidMount() {
        $("#top-bar").addClass('scrolled');
        $("#top-bar").removeClass('active');
        $("#top-bar").css({"background-color": "#000", "position": "sticky"});
        $("#top-bar").removeClass('d-none');
        $('#sidebarCollapse').removeClass('d-none');
    }
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col lg={12}>
                            <div className="mt-3">
                                <h2>About Page</h2>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div style={{height: 750}}></div>
            </React.Fragment>
        );
    }
}

export default About;
