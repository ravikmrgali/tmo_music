import React from 'react';
import $ from 'jquery';
import {Container, Row, Col} from 'react-bootstrap';

class Notfound extends React.Component {
    componentDidMount() {
        $("#top-bar").addClass('scrolled');
        $("#top-bar").removeClass('active');
        $("#top-bar").css("position", "sticky");
        $("#top-bar").removeClass('d-none');
        $('#sidebarCollapse').removeClass('d-none');
    }
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <h2>Page Not Found</h2>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default Notfound;
