import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import $ from 'jquery';

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isTop: false
        };
        this.onScroll = this.onScroll.bind(this);
    }

    componentDidMount() {
        // let clientHeight = document.getElementById('intro').offsetHeight;
        document.addEventListener('scroll', () => {
            const isTop = window.scrollY > 150;
            if (isTop !== this.state.isTop) {
                this.onScroll(isTop);
            }
        });
        $("#top-bar").removeClass('active');
    }

    onScroll(isTop) {
        this.setState({ isTop });
    }
    componentWillMount() {
        $(document).ready(function () {
            $("#sidebarCollapse").click(function (e) {
                $("#top-bar").toggleClass('active');
                // e.stopPropagation();
            });
            $("#top-bar").removeClass('active');

            $(document).on('click', function (event) {
                if (!$(event.target).closest('#top-bar').length) {
                    $("#top-bar").removeClass('active');
                    // Hide the menus.
                }
            });

            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#backToTop').fadeIn('slow');
                } else {
                    $('#backToTop').fadeOut('slow');
                }
            });
            $('#backToTop').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 900);
                return false;
            });
        });
    }
    render() {
        return (
            <React.Fragment>
                <div id="top-bar" className={this.state.isTop ? 'scrolled' : ''}>
                    <header className="top-header">
                            <div className="logo">
                                <Link to="/">FilesUpload</Link>
                            </div>
                            <div className="nav-right ml-auto">
                                <Link to="/login" className="login-link">Login</Link>
                                <a href="javascript:void(0)" id="sidebarCollapse" className=" menu-icon" onClick={this.handleClick}>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                            </div>
                    </header>
                    <div className="menu menus">
                        <ul className="list">
                            {/* <li><Link to='/' >Home</Link></li> */}
                            <li><NavLink exact activeClassName="active" to='/dashboard'>Dashboard</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/about'>About</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/contact'>Contact</NavLink></li>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" id="backToTop"><i className="fas fa-long-arrow-alt-up"></i></a>
                </div>
                {/* <div className="header">
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className="d-flex">
                                    <Link to='/' className="brand">React</Link>
                                    <div id="side-menu" className="menu menus">
                                        <ul className="list">
                                            <li><Link to='/' >Home</Link></li>
                                            <li><Link to='/about'>About</Link></li>
                                            <li><Link to='/contact'>Contact</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div> */}
            </React.Fragment>
        );
    }
}

export default Menu;


