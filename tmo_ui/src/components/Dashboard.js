import React from 'react';
import $ from 'jquery';
import {Container, Row, Col, Form, Button} from 'react-bootstrap';

class Dashboard extends React.Component {
    componentDidMount(){
        $("#top-bar").addClass('scrolled');
        $(document).ready(function(){
            $(window).scroll(function() {    
                var scroll = $(window).scrollTop();    
                if (scroll == 0) {
                    $("#top-bar").addClass("scrolled");
                }
            });
        });
        $("#top-bar").removeClass('active');
        $("#top-bar").css({"background-color": "#000", "position": "sticky"});
        $("#top-bar").removeClass('d-none');
        $(".login-link").text('Logout');
        $('#sidebarCollapse').removeClass('d-none');
    }
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col md={12}>
                            <div className="mt-3 text-center mb-4">
                                <h2>Dashboard Forms</h2>
                            </div>
                        </Col>
                        <Col lg="6">
                            <div className="form-box">
                                <h4 className="h4 mb-3">Normal Form</h4>
                                <Form>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" />
                                        <Form.Text className="text-muted">
                                            We'll never share your email with anyone else.
                                        </Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicChecbox">
                                        <Form.Check type="checkbox" label="Check me out" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                    <div className="mb-4"></div>
                                    {['checkbox', 'radio'].map(type => (
                                        <div key={`custom-${type}`} className="mb-3">
                                            <Form.Check
                                                custom
                                                type={type}
                                                id={`custom-${type}-1`}
                                                label={`Check this custom ${type}`}
                                                name={`normal-options-${type}`}
                                            />
                                            <Form.Check
                                                custom
                                                type={type}
                                                id={`custom-${type}-2`}
                                                label={`Check this custom ${type}`}
                                                name={`normal-options-${type}`}
                                            />
                                            <Form.Check
                                                custom
                                                disabled
                                                type={type}
                                                label={`disabled ${type}`}
                                                id={`disabled-custom-${type}-3`}
                                                name={`normal-options-${type}`}
                                            />
                                        </div>
                                    ))}
                                </Form>
                            </div>
                        </Col>
                        <Col lg="6">
                            <div className="form-box">
                                <h4 className="h4 mb-3">Inline Form</h4>
                                <Form>
                                    <Form.Group as={Row} controlId="formPlaintextEmail">
                                        <Form.Label column sm="2">
                                            Email
                                        </Form.Label>
                                        <Col sm="10">
                                            <Form.Control type="email" placeholder="Email" />
                                            <Form.Text className="text-muted">
                                                We'll never share your email with anyone else.
                                            </Form.Text>
                                        </Col>
                                    </Form.Group>

                                    <Form.Group as={Row} controlId="formPlaintextPassword">
                                        <Form.Label column sm="2">
                                            Password
                                        </Form.Label>
                                        <Col sm="10">
                                            <Form.Control type="password" placeholder="Password" />
                                        </Col>
                                    </Form.Group>
                                    {['checkbox', 'radio'].map(type => (
                                        <div key={`custom-inline-${type}`} className="mb-3">
                                            <Form.Check
                                                custom
                                                inline
                                                label="1"
                                                type={type}
                                                id={`custom-inline-${type}-1`}
                                                name={`inline-options-${type}`}
                                            />
                                            <Form.Check
                                                custom
                                                inline
                                                label="2"
                                                type={type}
                                                id={`custom-inline-${type}-2`}
                                                name={`inline-options-${type}`}
                                            />
                                            <Form.Check
                                                custom
                                                inline
                                                disabled
                                                label="3 (disabled)"
                                                type={type}
                                                id={`custom-inline-${type}-3`}
                                                name={`inline-options-${type}`}
                                            />
                                        </div>
                                    ))}
                                </Form>
                            </div>
                        </Col>

                        <Col lg={12}>
                            <div className="form-box mt-4">
                                <h4 className="h4 mb-3">Another Form Type</h4>
                                <Form>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridEmail">
                                            <Form.Label>Email</Form.Label>
                                            <Form.Control type="email" placeholder="Enter email" />
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridPassword">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type="password" placeholder="Password" />
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Group controlId="formGridAddress1">
                                        <Form.Label>Address</Form.Label>
                                        <Form.Control placeholder="1234 Main St" />
                                    </Form.Group>

                                    <Form.Group controlId="formGridAddress2">
                                        <Form.Label>Address 2</Form.Label>
                                        <Form.Control placeholder="Apartment, studio, or floor" />
                                    </Form.Group>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCity">
                                            <Form.Label>City</Form.Label>
                                            <Form.Control />
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridState">
                                            <Form.Label>State</Form.Label>
                                            <Form.Control as="select">
                                                <option>Choose...</option>
                                                <option>...</option>
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridZip">
                                            <Form.Label>Zip</Form.Label>
                                            <Form.Control />
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Group id="formGridCheckbox">
                                        <Form.Check type="checkbox" label="Check me out" />
                                    </Form.Group>

                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div style={{height: 750}}></div>
            </React.Fragment>
        );
    }
}

export default Dashboard;
