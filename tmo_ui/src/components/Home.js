import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import $ from 'jquery';

import OwlCarousel from 'react-owl-carousel3';
import wt1 from '../assets/images/wt1.jpg';
import wt2 from '../assets/images/wt2.jpg';
import wt3 from '../assets/images/wt3.jpg';
import wt4 from '../assets/images/wt4.jpg';
import wt5 from '../assets/images/wt5.jpg';
import wt6 from '../assets/images/wt6.jpg';
import wt7 from '../assets/images/wt7.jpg';
import wt8 from '../assets/images/wt8.jpg';
import wt9 from '../assets/images/wt9.jpg';
import wt10 from '../assets/images/wt10.jpg';
import wt11 from '../assets/images/wt11.jpg';
import wt12 from '../assets/images/wt12.jpg';
import ad1 from '../assets/audio/dummy-audio.mp3';
import okImg from '../assets/images/ok.png';
import noImg from '../assets/images/no.png';
import pa1 from '../assets/images/pa1.jpg';
import pa2 from '../assets/images/pa2.jpg';
import pa3 from '../assets/images/pa3.jpg';
import pa4 from '../assets/images/pa4.jpg';
import pa5 from '../assets/images/pa5.jpg';
import pa6 from '../assets/images/pa6.jpg';
import pa7 from '../assets/images/pa7.jpg';

export default class Home extends React.Component{
    componentDidMount(){
        $("#top-bar").removeClass('active');
        $("#top-bar").removeClass('scrolled');
        $("#top-bar").css("position", "fixed");
        $("#top-bar").removeClass('d-none');
        $('#sidebarCollapse').addClass('d-none');
    }
    render(){
        return(
            <section className="home">
                <div className="banner parallax overlay">
                    <div className="inner">
                        <p className="lead">let music directors know about you</p>
                        <h2 className="h2 text-white">Sing Loud</h2>
                    </div>
                </div>
                {/* -------------------- Banner end ----------------- */}

                <div className="who section">
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className="sec-title text-center mb-5">
                                    <h2 className="h2">Who is listening you</h2>
                                </div>
                                <OwlCarousel margin={30} loop nav autoplay navText={['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']} items={5}>
                                    <div className="item">
                                        <img src={wt1} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt2} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt3} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt4} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt5} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt6} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt7} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                    <div className="item">
                                        <img src={wt8} alt="Who Slide Image" />
                                        <div className="text-center wt-info">
                                            <a href="javascript:void(0)"><h4 className="h5 text-uppercase">the cure</h4></a>
                                            <p>Second Song</p>
                                        </div>
                                    </div>
                                </OwlCarousel>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* -------------------- Who end ----------------- */}

                <div className="what section parallax overlay">
                    <Container>
                        <Row>
                            <Col md={8}>
                                <div className="sec-title mb-4">
                                    <h2 className="h2 text-white">Who is listening you</h2>
                                </div>
                                <div className="content text-white mb-5">
                                    <p>... We Process your vocals with Analog gear.</p>
                                    <p>... Add minimal BGM to your vocals</p>
                                    <p>... Add maximum BGM to your vocals</p>
                                    <p>... Lift your vocal presence to big label standards</p>
                                </div>
                            </Col>
                            <Col md={6}>
                                <div className="play-area">
                                    <div class="song-name">
                                        <p>Before Process</p>
                                    </div>
                                    <audio preload="auto" controls>
                                        <source src={ad1} />
                                    </audio>
                                </div>
                            </Col>
                            <Col md={6}>
                                <div className="play-area">
                                    <div class="song-name">
                                        <p>After Process</p>
                                    </div>
                                    <audio preload="auto" controls>
                                        <source src={ad1} />
                                    </audio>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* -------------------- What end ----------------- */}

                <div className="premium-acc section bg-gray">
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className="sec-title text-center mb-5">
                                    <h2 className="h2">What is Premium Account</h2>
                                    <p>See what’s new</p>
                                </div>
                            </Col>
                            <Col lg={6} xl={{ span: 4, offset: 2 }}>
                                <div className="pr-acc-item">
                                    <div className="sec-title mb-4">
                                        <h2 className="h2 fs-18">Free Account</h2>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={okImg} alt="Ok Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Vocal Uploads</h6>
                                                <p>5 per month</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={noImg} alt="No Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Vocal Uploads</h6>
                                                <p>5 per month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={6} xl={4}>
                                <div className="pr-acc-item">
                                    <div className="sec-title mb-4">
                                        <h2 className="h2 fs-18">Premium Account</h2>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={okImg} alt="Ok Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Vocal Uploads</h6>
                                                <p>5 per month</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={noImg} alt="No Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Vocal Uploads</h6>
                                                <p>5 per month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* -------------------- Premium section end ----------------- */}

                <div className="whats-new section">
                    <Container>
                        <Row>
                            <Col lg={4} md={4}>
                                <div className="weeks-top ft-three-column">
                                    <div className="sec-title mb-4">
                                        <p>See what’s new</p>
                                        <h2 className="h2 fs-18">This week’s top</h2>
                                    </div>

                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt1} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Vocal Uploads</h6>
                                                <p>5 per month</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt2} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">William Parker</h6>
                                                <p>In my mind</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt3} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">SAM SMITH</h6>
                                                <p>Underground</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt4} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">THE MUSIC BAND</h6>
                                                <p>Songs and stuff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt5} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">CRISTINNE SMITH</h6>
                                                <p>My Music</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex">
                                        <div className="single-part d-flex">
                                            <div className="thumbnail-small">
                                                <img src={wt6} alt="what’s New Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">CREATIVE LYRICS</h6>
                                                <p>Underground</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            
                            <Col lg={4} md={4}>
                                <div className="new-hits ft-three-column">
                                    <div className="sec-title mb-4">
                                        <p>See what’s new</p>
                                        <h2 className="h2 fs-18">new hits</h2>
                                    </div>

                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt7} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">SAM SMITH</h6>
                                                <p>Underground</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt8} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">POWER PLAY</h6>
                                                <p>In my mind</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt9} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">CRISTINNE SMITH</h6>
                                                <p>My Music</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt10} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">THE MUSIC BAND</h6>
                                                <p>Underground</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt11} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">CREATIVE LYRICS</h6>
                                                <p>Songs and stuff</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={wt12} alt="New Hits Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">THE CULTURE</h6>
                                                <p>Pop Songs</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            
                            <Col lg={4} md={4}>
                                <div className="popular-artist ft-three-column">
                                    <div className="sec-title mb-4">
                                        <p>See what’s new</p>
                                        <h2 className="h2 fs-18">popular artist</h2>
                                    </div>

                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa1} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Sam Smith</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa2} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">William Parker</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa3} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Jessica Walsh</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa4} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Tha Stoves</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa5} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">DJ Ajay</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa6} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Radio Vibez</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="single-item d-flex align-items-center justify-content-between">
                                        <div className="single-part d-flex align-items-center">
                                            <div className="thumbnail-small">
                                                <img src={pa7} alt="Popular Artist Image" className="img-fluid" />
                                            </div>
                                            <div className="content">
                                                <h6 className="h6 text-uppercase mb-0">Music 4u</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* -------------------- What's New end ----------------- */}

                <footer className="footer">
                    <Container>
                        <Row>
                            <Col lg={12}>
                                <div className="copyright">
                                    <a href="javascript:void(0)" className="footer-logo">FilesUpload</a>
                                    <p className="mb-0">Copyright ©2019 All rights reserved.</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </footer>

            </section>
        );
    }
}