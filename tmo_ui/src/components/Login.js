import React from 'react';
import $ from 'jquery';
import {
	withRouter
} from 'react-router-dom';
class Login extends React.Component{
    
    componentDidMount(){
        $("#top-bar").addClass('d-none');
        $(".login-link").text('Login');
    }
    submitForm (e) {
		e.preventDefault()
		this.props.history.push('/dashboard');
	}
    render(){
        return(
            <section className="login-wrap">
                <div className="login-inner">
                    <span className="login-logo text-center d-block mb-4"><i className="fas fa-industry"></i></span>
                    <form onSubmit={this.submitForm.bind(this)}>
                        <div className="form-group">
                            <input type="text" placeholder="Username" className="form-control" />
                            <i className="fas fa-user icon"></i>
                            <div className="line"></div>
                        </div>
                        <div className="form-group">
                            <input type="password" placeholder="Password" className="form-control" />
                            <i className="fas fa-lock icon"></i>
                            <div className="line"></div>
                        </div>
                        <div class="form-group form-check mb-4">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" /> Remember me
                            </label>
                        </div>
                        <div className="text-center mb-5">
                            <button type="submit" className="btn btn-white"><i className="fas fa-sign-in-alt"></i>&nbsp; Login</button>
                        </div>
                        <div className="text-center">
                            <a href="javascript:void(0)" className="forgot-link text-white">Forgot Password?</a>
                        </div>
                    </form>
                </div>
                {/* <div style={{height: 750}}></div> */}
            </section>
        );
    }
}

export default withRouter(Login);