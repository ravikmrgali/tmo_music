package com.tmo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tmo.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {
	
	@Query("select user from User user where user.userId=:userId")
	User findOne(@Param("userId") Long id);
	
}
