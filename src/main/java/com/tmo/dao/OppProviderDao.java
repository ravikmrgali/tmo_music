package com.tmo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tmo.model.OpportunityProviders;

@Repository
public interface OppProviderDao extends JpaRepository<OpportunityProviders, Long>{

}
