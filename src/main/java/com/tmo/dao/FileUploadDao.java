package com.tmo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmo.model.FileUpload;

public interface FileUploadDao extends JpaRepository<FileUpload, Long> {

}
