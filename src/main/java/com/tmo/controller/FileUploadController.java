package com.tmo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tmo.dao.FileUploadDao;
import com.tmo.model.FileUpload;

@RestController
@RequestMapping("file")
public class FileUploadController {

	@Autowired
	private FileUploadDao fileUploadDao;
	private static String UPLOADED_FOLDER = "G:\\SaveFiles\\";

	@PostMapping("/uploadFile")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile) {

		if (uploadfile.isEmpty()) {
			return new ResponseEntity("please select a file!", HttpStatus.OK);
		}

		try {

			String filePath = saveUploadedFiles(Arrays.asList(uploadfile));

			FileUpload fileUpload = new FileUpload();

			fileUpload.setFilePath(filePath);
			System.out.println("sssss :" + fileUpload + "path   " + filePath);

			fileUploadDao.save(fileUpload);

		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity("Successfully uploaded - " + uploadfile.getOriginalFilename(), new HttpHeaders(),
				HttpStatus.OK);

	}

	private String saveUploadedFiles(List<MultipartFile> files) throws IOException {

		String filePath = "";

		for (MultipartFile file : files) {

			if (file.isEmpty()) {
				continue; // next pls
			}

			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			System.out.println("Path :" + path);
			System.out.println("filename  :  " + file.getOriginalFilename());
			Files.write(path, bytes);

			filePath = UPLOADED_FOLDER + file.getOriginalFilename();

		}

		return filePath;

	}
}
