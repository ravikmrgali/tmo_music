package com.tmo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmo.dao.OppDao;
import com.tmo.model.Opportunities;

@RestController
@RequestMapping("/opportunities")
public class OppController {

	@Autowired
	private OppDao oppDao;

	@RequestMapping(value = "/getListOfOpportunities", method = RequestMethod.GET, produces = "application/json")
	public Optional<Opportunities> getUserById(@RequestParam(value = "oppId") String oppId) {
		return oppDao.findById(Long.parseLong(oppId));
	}

	@RequestMapping(value = "/saveOpp", method = RequestMethod.POST)
	public String saveOpportunities(@RequestBody Opportunities requestOpp) {
		oppDao.save(requestOpp);
		return "Success";
	}
	
	@RequestMapping(value = "/updateOpp", method = RequestMethod.POST)
    public String updateOpportunitiesById(@RequestBody Opportunities requestOpp){
		oppDao.save(requestOpp);
		return "Success";
    }

}
