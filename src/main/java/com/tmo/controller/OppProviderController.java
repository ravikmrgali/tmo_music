package com.tmo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmo.dao.OppProviderDao;
import com.tmo.model.OpportunityProviders;

@RestController
@RequestMapping(value="/oppProvider")
public class OppProviderController {
	
	@Autowired
	private OppProviderDao oppProviderDao;
	
	@RequestMapping(value = "/getoppProvider", method = RequestMethod.GET, produces = "application/json")
    public Optional<OpportunityProviders> getUserById(@RequestParam(value = "providerId") String providerId){
            return oppProviderDao.findById(Long.parseLong(providerId));
    }
	
	@RequestMapping(value = "/saveOppProvider", method = RequestMethod.POST)
    public String saveUser(@RequestBody OpportunityProviders requestOppProvider){
		oppProviderDao.save(requestOppProvider);
		return "Success";
    }
	
	@RequestMapping(value = "/updateOppProvider", method = RequestMethod.POST)
    public String updateUserById(@RequestBody OpportunityProviders requestOppProvider){
		oppProviderDao.save(requestOppProvider);
		return "Success";
    }

}
