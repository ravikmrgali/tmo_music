package com.tmo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmo.dao.UserDao;
import com.tmo.model.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value = "/getUser", method = RequestMethod.GET, produces = "application/json")
    public Optional<User> getUserById(@RequestParam(value = "userId") String userId){
            return userDao.findById(Long.parseLong(userId));
    }
	
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public String saveUser(@RequestBody User requestUser){
		//User user = userDao.findOne(userId);
		userDao.save(requestUser);
		//userDao.update(requestUser.getUserName());
		//user.setUserName(requestUser.getUserName());
		
		return "Success";
    }
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public String updateUserById(@RequestBody User requestUser){
		userDao.save(requestUser);
		return "Success";
    }
	
}
