package com.tmo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="opp")
public class OpportunityProviders implements Serializable{
	
	private static final long serialVersionUID = -6246376097550304453L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long providerId;
	private String providerName;
	private String providerSkillCode;

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderSkillCode() {
		return providerSkillCode;
	}

	public void setProviderSkillCode(String providerSkillCode) {
		this.providerSkillCode = providerSkillCode;
	}

}
