package com.tmo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "opportunities")
public class Opportunities {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long oppId;
	private String oppProviderId;
	private String oppShortDescription;
	private String oppDescription;
	private Double isPremium;
	public long getOppId() {
		return oppId;
	}
	public void setOppId(long oppId) {
		this.oppId = oppId;
	}
	public String getOppProviderId() {
		return oppProviderId;
	}
	public void setOppProviderId(String oppProviderId) {
		this.oppProviderId = oppProviderId;
	}
	public String getOppShortDescription() {
		return oppShortDescription;
	}
	public void setOppShortDescription(String oppShortDescription) {
		this.oppShortDescription = oppShortDescription;
	}
	public String getOppDescription() {
		return oppDescription;
	}
	public void setOppDescription(String oppDescription) {
		this.oppDescription = oppDescription;
	}
	public Double getIsPremium() {
		return isPremium;
	}
	public void setIsPremium(Double isPremium) {
		this.isPremium = isPremium;
	}
	
}
