package com.tmo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
//@ComponentScan(value="com.tmo.service")
public class TmoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmoServiceApplication.class, args);
	}
}
